<?php

namespace App\Enums;

/**
 * This is the Status enum of the Task Model
 */
enum StatusEnum: string {
  case TODO = 'TODO';
  case DONE = 'DONE';
}
