<?php

namespace App\GraphQL\Queries\Task;

use App\GraphQL\Queries\Task\BaseTaskQuery;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class TaskQuery extends BaseTaskQuery
{
    protected $attributes = [
        'name' => 'task',
    ];

    public function type(): Type
    {
        return GraphQL::type('Task');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskId($args['id']);
        $task = $this->task();
        if ($task) {
            return $task;
        } else {
            abort(403, 'Access denied');
        }
    }
}
