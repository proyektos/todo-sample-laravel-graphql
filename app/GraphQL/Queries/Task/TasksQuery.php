<?php

namespace App\GraphQL\Queries\Task;

use App\GraphQL\Queries\Task\BaseTaskQuery;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class TasksQuery extends BaseTaskQuery
{
    protected $attributes = [
        'name' => 'tasks',
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('Task'));
    }

    public function args(): array
    {
        return [
            'taskListId' => [
            'name' => 'taskListId',
            'type' => Type::int(),
            'rules' => ['required']
        ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->tasks($args['taskListId']);
    }
}
