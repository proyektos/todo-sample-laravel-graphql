<?php

namespace App\GraphQL\Queries\Task;

use App\Models\Task;
use App\Models\TaskList;
use App\GraphQL\Queries\AuthenticatedQuery;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class BaseTaskQuery extends AuthenticatedQuery
{
    protected $taskId;

    protected function setTaskId($id) {
        $this->taskId = $id;
    }

    protected function task() {
        $task = Task::findOrFail($this->taskId);
        $taskList = $task->taskList;
        if ($task
            && $taskList->owner_id == $this->user()->id) {
                return $task;
        } else {
            return null;
        }

    }

    protected function tasks($checkTaskList) {
        // $taskList = TaskList::where([
        //     ['id', '=', $checkTaskList],
        //     ['owner_id', '=', $this->user()->id],
        // ])->firstOrFail();  
        
        // return $taskList->tasks;
        $taskList = TaskList::where([
            ['id', '=', $checkTaskList],
            ['owner_id', '=', $this->user()->id],
        ])->firstOrFail();  
        
        return $taskList->tasks;
    }

}
