<?php

namespace App\GraphQL\Queries;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class AuthenticatedQuery extends Query
{
    protected function user()
    {
        return request()->user();
    }

    public function type(): Type {}

}
