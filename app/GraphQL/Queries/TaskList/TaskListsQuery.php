<?php

namespace App\GraphQL\Queries\TaskList;

use App\GraphQL\Queries\TaskList\BaseTaskListQuery;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class TaskListsQuery extends BaseTaskListQuery
{
    protected $attributes = [
        'name' => 'taskLists',
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('TaskList'));
    }

    public function resolve($root, $args)
    {
        return $this->taskLists();
    }
}
