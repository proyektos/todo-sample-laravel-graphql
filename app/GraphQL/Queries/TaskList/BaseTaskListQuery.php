<?php

namespace App\GraphQL\Queries\TaskList;

use App\Models\TaskList;
use App\GraphQL\Queries\AuthenticatedQuery;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class BaseTaskListQuery extends AuthenticatedQuery
{
    protected $taskListId;

    protected function setTaskListId($id) {
        $this->taskListId = $id;
    }

    protected function taskList() {
        $taskList = TaskList::where([
            ['id', '=', $this->taskListId],
            ['owner_id', '=', $this->user()->id],
        ])->firstOrFail();

        return $taskList;
    }

    protected function taskLists() {
        $taskLists = TaskList::where('owner_id', $this->user()->id)->get();
        return $taskLists;
        // return $user->taskLists;
    }


}
