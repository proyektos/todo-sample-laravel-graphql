<?php

namespace App\GraphQL\Queries\TaskList;

use App\GraphQL\Queries\TaskList\BaseTaskListQuery;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class TaskListQuery extends BaseTaskListQuery
{
    protected $attributes = [
        'name' => 'taskList',
    ];

    public function type(): Type
    {
        return GraphQL::type('TaskList');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskListId($args['id']);
        $taskList = $this->taskList();
        if ($taskList) {
            return $taskList;
        } else {
            abort(403, 'Access denied');
        }
    }
}