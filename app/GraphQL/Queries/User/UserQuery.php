<?php

namespace App\GraphQL\Queries\User;

use App\Models\User;
use App\GraphQL\Queries\AuthenticatedQuery;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UserQuery extends AuthenticatedQuery
{
    protected $attributes = [
        'name' => 'user',
        'description' => 'Retrieves current user logged in',
    ];

    public function type(): Type
    {
        return GraphQL::type('User');
    }

    public function resolve($root, $args)
    {
        $user = $this->user();

        return [
            'id' => $user->id,
            'username' => $user->username,
        ];
    }
}
