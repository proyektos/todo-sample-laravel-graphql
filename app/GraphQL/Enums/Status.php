<?php

namespace App\GraphQL\Enums;

use Rebing\GraphQL\Support\EnumType;
use App\Enums\StatusEnum;

class Status extends EnumType
{
    protected $attributes = [
        'name' => 'Status',
        'description' => 'The Status of a Task',
        'values' => [
            'TODO' => StatusEnum::TODO,
            'DONE' => StatusEnum::DONE
        ],
    ];
}
