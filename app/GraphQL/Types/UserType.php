<?php

namespace App\GraphQL\Types;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'Collection of users',
        'model' => User::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'ID of user',
            ],
            'username' => [
                'type' => Type::string(),
                'description' => 'Username of the user',
            ],
            'token' => [
                'type' => Type::string(),
                'description' => 'Token of the user',
            ],
        ];
    }
}
