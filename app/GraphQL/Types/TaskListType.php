<?php

namespace App\GraphQL\Types;

use App\Models\TaskList;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TaskListType extends GraphQLType
{
    protected $attributes = [
        'name' => 'TaskList',
        'description' => 'Collection of Task Lists owned by a User',
        'model' => TaskList::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'ID of task list'
            ],
            'label' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Label of the task list'
            ],
            'owner' => [
                'type' => GraphQL::type('User'),
                'description' => 'The owner of the task list'
            ],
            'tasks' => [
                'type' => Type::listOf(GraphQL::type('Task')),
                'description' => 'List of tasks'
            ]
        ];
    }
}
