<?php

namespace App\GraphQL\Types;

use App\Models\Task;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TaskType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Task',
        'description' => 'Collection of tasks associated to a task list',
        'model' => Task::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'ID of task'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Title of the task'
            ],
            'status' => [
                'type' => GraphQL::type('Status'),
                'description' => 'Status of task'
            ],
            'taskList' => [
                'type' => GraphQL::type('TaskList'),
                'description' => 'The list where the task belongs'
            ]
        ];
    }
}
