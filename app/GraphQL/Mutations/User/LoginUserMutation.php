<?php

namespace App\GraphQL\Mutations\User;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Support\Facades\Hash;
use App\Models\Sanctum\PersonalAccessToken;
use Laravel\Sanctum\Sanctum;

class LoginUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'Logs a user in'
    ];

    public function type(): Type
    {
        return GraphQL::type('User');
    }

    public function args(): array
    {
        return [
            'username' => [
                'name' => 'username',
                'type' => Type::nonNull(Type::string()),
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::nonNull(Type::string()), 
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $matchingUser = User::where('username', $args['username'])->first();

        if ($matchingUser && Hash::check($args['password'], $matchingUser->getAuthPassword())) {
            $userToken = $matchingUser->createToken("{$matchingUser->id}-token");

            return [
                'id' => $matchingUser->id,
                'token' => $userToken->plainTextToken,
                'username' => $matchingUser->username,
            ];
        } else {
            abort(401, 'Incorrect username or password');
        }
    }
}