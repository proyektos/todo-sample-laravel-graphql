<?php

namespace App\GraphQL\Mutations\User;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

/* made in anticipation for future admin use, otherwise unused */
class DeleteUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'deleteUser',
        'description' => 'Deletes a user'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'delete' => [
                'name' => 'delete',
                'type' => Type::nonNull(Type::boolean())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if ($args['delete']) {
            $user = User::findOrFail(request()->user()->id);
            return $user->delete() ? true : false;
        }
    }
}