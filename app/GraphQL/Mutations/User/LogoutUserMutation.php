<?php

namespace App\GraphQL\Mutations\User;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

use Auth;
use Closure;

class LogoutUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'logout',
        'description' => 'Logs a user out'
    ];

    public function type(): Type
    {
        return GraphQL::type('User');
    }

    public function resolve($root, $args)
    {
        $currentLoggedIn = request()->user();

        if ($currentLoggedIn) {
            return $currentLoggedIn->currentAccessToken()->delete();
        } else {
            return [
                'username' => 'false',
            ];
        }
    }
}