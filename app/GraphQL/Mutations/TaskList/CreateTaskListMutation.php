<?php

namespace App\GraphQL\Mutations\TaskList;

use App\Models\TaskList;
use App\GraphQL\Mutations\TaskList\BaseTaskListMutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateTaskListMutation extends BaseTaskListMutation
{
    protected $attributes = [
        'name' => 'createTaskList',
        'description' => 'Creates a task list',
    ];

    public function type(): Type
    {
        return GraphQL::type('TaskList');
    }

    public function args(): array
    {
        return [
            'label' => [
                'name' => 'label',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $insert = [
            'owner_id' => $this->user()->id,
            'label' => $args['label'],
        ];

        $taskList = new TaskList();
        $taskList->fill($insert);
        $taskList->save();

        return $taskList;
    }
}