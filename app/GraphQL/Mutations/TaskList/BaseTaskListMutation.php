<?php

namespace App\GraphQL\Mutations\TaskList;

use App\Models\TaskList;
use App\GraphQL\Mutations\AuthenticatedMutation;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class BaseTaskListMutation extends AuthenticatedMutation
{
    protected $taskListId;

    protected function setTaskListId($id) {
        $this->taskListId = $id;
    }

    protected function taskList() {
        $taskList = TaskList::where([
            ['id', '=', $this->taskListId],
            ['owner_id', '=', $this->user()->id],
        ])->firstOrFail();

        return $taskList;
    }


}
