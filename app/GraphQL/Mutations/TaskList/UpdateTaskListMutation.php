<?php

namespace App\GraphQL\Mutations\TaskList;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Mutations\TaskList\BaseTaskListMutation;

class UpdateTaskListMutation extends BaseTaskListMutation
{
    protected $attributes = [
        'name' => 'updateTaskList',
        'description' => 'Updates a task list'
    ];

    public function type(): Type
    {
        return GraphQL::type('TaskList');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
            ],
            'label' => [
                'name' => 'label',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskListId($args['id']);
        $taskList = $this->taskList();

        if ($taskList) {
            $taskList->fill($args);
            $taskList->save();

            return $taskList;
        } else {
            abort(403, 'Access denied');
        }
    }
}