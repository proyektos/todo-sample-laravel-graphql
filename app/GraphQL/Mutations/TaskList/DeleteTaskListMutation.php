<?php

namespace App\GraphQL\Mutations\TaskList;

use GraphQL\Type\Definition\Type;
use App\GraphQL\Mutations\TaskList\BaseTaskListMutation;

class DeleteTaskListMutation extends BaseTaskListMutation
{
    protected $attributes = [
        'name' => 'deleteTaskList',
        'description' => 'Deletes a task list'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['exists:task_lists']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskListId($args['id']);

        return  $this->taskList()->delete() ? true : false;
    }
}