<?php

namespace App\GraphQL\Mutations;

use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class AuthenticatedMutation extends Mutation
{
    protected function user()
    {
        return request()->user();
    }

    public function type(): Type {}

}
