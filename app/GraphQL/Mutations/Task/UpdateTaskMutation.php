<?php

namespace App\GraphQL\Mutations\Task;

use App\Models\Task;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Mutations\Task\BaseTaskMutation;

class UpdateTaskMutation extends BaseTaskMutation
{
    protected $attributes = [
        'name' => 'updateTask',
        'description' => 'Updates a task'
    ];

    public function type(): Type
    {
        return GraphQL::type('Task');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string()
            ],
            'status' => [
                'name' => 'status',
                'type' => GraphQL::type('Status')
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskId($args['id']);
        $task = $this->task();

        if ($task) {
            $task->fill($args);
            $task->save();
            return $task;
        } else {
            abort(403, 'Access denied');
        }
    }
}