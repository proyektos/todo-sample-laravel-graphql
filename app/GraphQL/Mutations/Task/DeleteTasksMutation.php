<?php

namespace App\GraphQL\Mutations\Task;

use App\Models\Task;
use App\Enums\Status;
use Carbon\Carbon;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Mutations\Task\BaseTaskMutation;

class DeleteTasksMutation extends BaseTaskMutation
{
    protected $attributes = [
        'name' => 'deleteTasks',
        'description' => 'Deletes a set of tasks',
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'ids' => [
                'name' => 'ids',
                'type' => Type::nonNull(Type::listOf(Type::int())),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $tasks = $this->tasks($args['ids'], false);

        if ($tasks) {
            return $tasks->update(['t.deleted_at' => Carbon::now()]) ? true : false;
        }
    }
}