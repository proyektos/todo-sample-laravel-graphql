<?php

namespace App\GraphQL\Mutations\Task;

use App\Models\Task;
use App\Enums\StatusEnum;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Mutations\Task\BaseTaskMutation;

class UpdateTasksMutation extends BaseTaskMutation
{
    protected $attributes = [
        'name' => 'updateTasks',
        'description' => 'Updates a set of tasks',
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('Task'));
    }

    public function args(): array
    {
        return [
            'ids' => [
                'name' => 'ids',
                'type' => Type::nonNull(Type::listOf(Type::int())),
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
            ],
            'status' => [
                'name' => 'status',
                'type' => GraphQL::type('Status'),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $tasks = $this->tasks($args['ids'], false);

        if ($tasks) {
            $update = array_filter($args, function($value, $key) {
                if ($key == 'ids') {
                    return null;
                }
                return !empty($value);
            }, ARRAY_FILTER_USE_BOTH);
            $updatedTasks = $tasks->update($update)->get();
            
            // have to repackage to ensure status is an enum Status
            $repackaged = array_map(function ($task) {
                return [
                    'id' => $task['id'],
                    'title' => $task['title'],
                    'status' => StatusEnum::from($task['status']),
                ];
            }, json_decode(json_encode($updatedTasks, true), true));

            return $repackaged;
        }
    }
}