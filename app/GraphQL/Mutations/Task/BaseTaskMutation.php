<?php

namespace App\GraphQL\Mutations\Task;

use App\Models\Task;
use App\Models\TaskList;
use App\GraphQL\Mutations\AuthenticatedMutation;
use Illuminate\Support\Facades\DB;
/**
 * To automatically retrieve and assign the logged in user
 */

abstract class BaseTaskMutation extends AuthenticatedMutation
{
    protected $taskId;

    protected function setTaskId($id) {
        $this->taskId = $id;
    }

    protected function task() {
        $task = Task::findOrFail($this->taskId);
        $taskList = $task->taskList;
        
        if ($task
            && $taskList->owner_id == $this->user()->id) {
                return $task;
        } else {
            return null;
        }

    }

    protected function tasks($ids, $shouldGet = true) {
        $tasks = Task::whereIn('id', $ids)->get();

        if ($tasks) {
            // paranoid checking if tasks to be updated are actually owned by user
            $ownerId = $this->user()->id;
            $results = DB::table('tasks as t')
                ->join('task_lists as tl', 'tl.id', '=', 't.task_list_id')
                ->select('t.*')
                ->whereIn('t.id', $ids)
                ->where('tl.owner_id', $ownerId);

            if ($shouldGet) {
                $results = $results->get();
            } else {
                $results = tap($results);
            }

            return $results;
        }

        return false;
    }

    /**
     * For new tasks.
     */
    protected function ownsTaskList($checkTaskList) {
        $taskList = TaskList::where([
            ['id', '=', $checkTaskList],
            ['owner_id', '=', $this->user()->id],
        ])->count();

        return $taskList > 0;
    }

}
