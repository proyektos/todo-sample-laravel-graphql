<?php

namespace App\GraphQL\Mutations\Task;

use GraphQL\Type\Definition\Type;
use App\GraphQL\Mutations\Task\BaseTaskMutation;

class DeleteTaskMutation extends BaseTaskMutation
{
    protected $attributes = [
        'name' => 'deleteTask',
        'description' => 'Deletes a task'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['exists:tasks']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $this->setTaskId($args['id']);
        $task = $this->task();

        if ($task) {
            return $task->delete() ? true : false;
        } else {
            abort(403, 'Access denied');
        }

    }
}