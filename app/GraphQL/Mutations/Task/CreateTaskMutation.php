<?php

namespace App\GraphQL\Mutations\Task;

use App\Models\Task;
use App\Enums\StatusEnum;
use App\GraphQL\Mutations\Task\BaseTaskMutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateTaskMutation extends BaseTaskMutation
{
    protected $attributes = [
        'name' => 'createTask',
        'description' => 'Creates a task',
    ];

    public function type(): Type
    {
        return GraphQL::type('Task');
    }

    public function args(): array
    {
        return [
            'taskListId' => [
                'name' => 'taskListId',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['exists:task_lists,id']
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        if ($this->ownsTaskList($args['taskListId'])) {
            $insert = [
                'task_list_id' => $args['taskListId'],
                'title' => $args['title'],
                'status' => StatusEnum::TODO,
            ];
    
            $task = new Task();
            $task->fill($insert);
            $task->save();
            return $task;
        } else {
            abort(403, 'Access denied');
        }
 
    }
}