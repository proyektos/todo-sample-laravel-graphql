<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\StatusEnum;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'task_list_id',
        'title',
        'status',
    ];
    
    protected function casts(): array {
        return [
            'status' => StatusEnum::class,
        ];
    }

    public function taskList() {
        return $this->belongsTo(TaskList::class, 'task_list_id', 'id');
    }
}
