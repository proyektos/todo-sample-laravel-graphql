<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskList extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'owner_id',
        'label',
    ];

    public function owner() {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }
}
