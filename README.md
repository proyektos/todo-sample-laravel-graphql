# TODO List Test

## Prerequisites
- PHP ^8.2.0
- Composer ^2.7.0
- Docker ^


## Installation instructions
- Clone repository
- cd into repo
- Execute `composer install` in the root directory
- Execute `php artisan sail:install`
  - Select `mysql` as your database
- Execute `./vendor/bin/sail up -d`
  - optional can alias `./vendor/bin/sail` to just `sail`
  - might need to change ports in the `docker-compose.yml` (`ports:`)
- Execute `sail artisan migrate`
- Execute `sail artisan db:seed`
- Run `docker ps` to see if the processes are running
