<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\TaskList;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'username' => 'user',
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);
        User::factory(3)->create();
        TaskList::factory(10)->create();
        Task::factory(35)->create();
    }
}
