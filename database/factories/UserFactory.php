<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{

    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $pass = Hash::make('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
        return [
            'username' => fake()->unique()->userName(),
            'password' => $pass,
            'remember_token' => Str::random(10),
        ];
    }
}
