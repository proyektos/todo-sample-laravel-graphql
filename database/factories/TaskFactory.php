<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Task;
use App\Models\TaskList;
use App\Enums\StatusEnum;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{

    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $taskListIds = TaskList::all()->pluck('id')->toArray();
        $statuses = [StatusEnum::TODO, StatusEnum::DONE];

        return [
            'title' => $this->faker->text(),
            'status' => $this->faker->randomElement($statuses),
            'task_list_id' => $this->faker->randomElement($taskListIds)
        ];
    }
}
